# coding: utf-8
# !/usr/bin/python3

import pytest
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options

serverHost = os.environ.get("SERVER_HOST")

def test_basic_headless_selenium_example():
    opts = Options()
    opts.headless = True
    driver = webdriver.Firefox(options=opts)
    driver.get(serverHost)
    assert "iKy - I Know You" in driver.title
    driver.close()
